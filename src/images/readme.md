#images source directory

Please see the gulp task `copy-assets` for the full list of file types currently set to be copied. If needed, add to the current list of file suffixes with a `|` mark:

```
jpg|jpeg|gif|png|svg|mp4|ogg|webm
```
