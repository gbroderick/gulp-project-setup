"use strict";
const constantMessage = "ES6 conversion test";

//https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Functions/Arrow_functions
var materials = [
  'Hydrogen',
  'Helium',
  'Lithium',
  'Beryllium'
];


var materialsLength3 = materials.map(material => material.length); //8,6,7,9
