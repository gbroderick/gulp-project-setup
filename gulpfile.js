//---GULP DEPENDENCIES---
//Node.js libraries
var del = require('del');
var path = require('path');
// var fs = require('fs'); //may need in future
// var merge = require('merge-stream'); //may need in future
var minimist = require('minimist');

//Gulp-only dependencies
var gulp = require('gulp');
var gutil = require('gulp-util');
var gulpif = require('gulp-if');
var pug = require('gulp-pug');
var rubysass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var babel = require('gulp-babel');

//---Pathing Variables---
var inputDir = "./src/";
var devOutput = "./dev/";
var distOutput = "./dist/";

//---- Minimist Output options ----- // for --arg passing to Node via command line
var knownOptions = {
  string : "out",
  default : {out : 'dev'} //will output dev if no `--out` arguments passed to node
}
var options = minimist(process.argv.slice(2), knownOptions);

function checkOutput(arg){
  if (options.out === "dev") {
    return devOutput;
  }
  else {
    return distOutput;
  }
}

//make check for output directory
var outputDir = checkOutput(options.out);
console.log("Outputting files to " + outputDir);


gulp.task('copy-html', function(){
  return gulp.src(path.join(inputDir, 'html_only/**/*.html'))
    .pipe(gulp.dest(path.join(outputDir)));
})


//---Pug to HTML Tasks---
gulp.task('pug-html', function() {
  return gulp.src(path.join(inputDir, 'pug/*.pug')) // To avoid processing `includes`, may have issue with extensions, however
    ////dev output
    .pipe(gulpif(options.out === 'dev', pug({
      // pretty: true //for gulp system testing only--apparently white space problems and will be soon deprecated (browser testing reccomended for HTML output)
    })))
    //// All other outputs
    .pipe(gulpif(options.out !=='dev', pug({
      compileDebug : false
    })))
    .pipe(gulp.dest(path.join(outputDir)));
});

//---JS Tasks---
gulp.task('lint', function() {
  return gulp.src(path.join(inputDir, '**/*.js')).pipe(jshint()).pipe(
      jshint.reporter('default'));
});

gulp.task('scripts', function() {
  if (options.out === "dev") {
    return gulp.src(path.join(inputDir, 'js/**/*.js'))
      .pipe(sourcemaps.init())
      .pipe(babel({
        presets: ['es2015']
      }))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(path.join(outputDir, 'js/')));
  }
  else {
    return gulp.src(path.join(inputDir, 'js/**/*.js'))
      //babel piping--- dev should have sourcemapping
      .pipe(babel({
        presets: ['es2015']
      }))
      .pipe(uglify())
      .pipe(gulp.dest(path.join(outputDir, 'js/')));
  }
});


//---Sass/SCSS to CSS Tasks---
gulp.task('styles', function() {
  //Stage Output
  if (options.out === "dev") {
    rubysass(path.join(inputDir, 'sass/compiler.scss'), {sourcemap: true, style: 'expanded'})
      .on('error', function(err) {
        console.error('Error', err.message);
      })
      .pipe(sourcemaps.write('sass-maps',{ //make external sass-maps-- see .gitignore
        includeContent: false,
        sourceRoot: 'source'
      }))
      .pipe(gulp.dest(outputDir))
  }
  //All other output
  else {
    return rubysass(path.join(inputDir, 'sass/compiler.scss'), {sourcemap: false, style: 'compressed'})
      .on('error', function(err) {
        console.error('Error', err.message);
      })
      .pipe(gulp.dest(outputDir))
  }
});
//---Asset Tasks---
gulp.task('copy-assets', function() {
  return gulp.src(path.join(inputDir, 'images/**/*.+(jpg|jpeg|gif|png|svg|mp4|ogg|webm)'))
    .pipe(gulp.dest(path.join(outputDir, "images/")));
});
//---Clean Tasks---
gulp.task('clean-dev', function(){
  del(devOutput);
});
gulp.task('clean-dist', function(){
  del(distOutput);
});
gulp.task('clean',['clean-dev', 'clean-dist']);


//---Single Output Combined Task---
gulp.task('out', ['styles', 'scripts', 'pug-html', 'copy-assets']); // for Pug only
// gulp.task('out', ['styles', 'scripts', 'copy-html', 'copy-assets']); // for HTML only


//---Watch Task---
gulp.task('watch', ['styles', 'scripts', 'pug-html', 'copy-assets'], function() {
    gulp.watch([inputDir + 'sass/**/*.scss', inputDir + 'sass/**/*.sass'], ['styles']);
    gulp.watch([inputDir + 'pug/**/*.pug'], ['pug-html']);
    gulp.watch([inputDir + 'js/**/*.js'], ['scripts']);
    gulp.watch([inputDir + 'images/**/*.+(jpg|jpeg|gif|png|svg|mp4|ogg|webm)'], ['copy-assets']);
});


//HTML only watch task
// gulp.task('watch', ['styles', 'scripts', 'copy-html', 'copy-assets'], function() {
//     gulp.watch([inputDir + 'sass/**/*.scss', inputDir + 'sass/**/*.sass'], ['styles']);
//     gulp.watch([inputDir + 'html_only/**/*.html'], ['copy-html']);
//     gulp.watch([inputDir + 'js/**/*.js'], ['scripts']);
//     gulp.watch([inputDir + 'images/**/*.+(jpg|jpeg|gif|png|svg|mp4|ogg|webm)'], ['copy-assets']);
// });


//---Default Watch---
gulp.task('default', ['watch']);
