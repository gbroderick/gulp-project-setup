#Gulp Project Template

----

A template for outputting quick front end experiments using Pug/Jade for HTML, Sass for CSS, and ES6 for JS.

---

##Installation

To install the gulp build system, install its dependencies through NPM. In the CLI, navigate to the project directory and enter in:

```
npm install
```

Bear in mind you may need to install [node / npm and gulp globally](http://blog.teamtreehouse.com/install-node-js-npm-mac). In particular you may possibly need to install gulp globablly with:
```
npm install gulp -g
```

---

##Usage

The input files before preprocessing should be available in the `src` directory. The actual processing of these files is done through Gulp. Depending on the `--out` argument given to Gulp, the output files will either be an uncompressed dev version (in `./dev/`) suitable for debugging, or a compressed, minified version in a `./dist/` folder. The default for most tasks is the uncompressed dev output.

###Outputting Debuggable Dev Files

To run a one-time output of uncompressed and sourcemapped dev files to `./dev/`, navigate to the project directory in your CLI and enter in:

```
gulp out
```

To run a 'watch' task that will track source file changes, simply enter the default gulp task:
```
gulp
```
On a Mac computer, you can end a watch task in the CLI by navigating to the set watch task and hitting `ctrl + c`.

Please note: the watch will not always automatically copy files that are newly added. Please either stop and restart the watch or run `gulp stage` to ensure all new files are caught by the appropriate gulp processes.


###Outputting Production Distribution Files

These compressed, no-sourcemaps files are output to `./dist/`.

To do a single output of these files:
```
gulp out --out dist
```
To start a watch that outputs to dist (not recommended :
```
gulp --out dist
```

##Clean Task
Delete all output directories with:
```
gulp clean
```
Please be aware when commiting to git that the dist and dev folders have been excluded in the .gitignore.

---

##Preprocessor Specifics

###Pug to HTML (optional)

The gulp tasks have the option of using the preproccessor white-space templating language to output HTML.

Formerly JadeJS, PugJS is an HTML templating language reducing tag closures to white space. It has some notable features like support for variables, conditionals, Mixins, and the insertion of code blocks with `includes`.

One important hurdle to note is that class and id attributes can be applied as though through a CSS selector, though other attributes use something similar to a Markdown sensibility to enter via . For example, this:

```
p#legalExplainer.side-text(data-text='generated')
```
will output in html to:
```html
<p id='legalExplainer' class='side-text' data-text='generated'></p>
```

Here is a [link to the docs](https://pugjs.org/api/getting-started.html). Please note that some of the fancier data manipulation aspects showcased in their docs seem to be somewhat in flux and may not play nice with certain templated directives.


###Sass to CSS

Sass/SCSS adds variable, mixin, and partials @import support among other key features to the style layer.

For a basic explainer on Sass and its structure, please refer to their [intro guide](http://sass-lang.com/guide).

Please note also that this project setup will likely mix SCSS and Sass. [Please refer to the Sass-lang guide on the difference between the two syntaxes](http://sass-lang.com/documentation/file.SCSS_FOR_SASS_USERS.html)

Also, [here is a link to the main reference documentation](http://sass-lang.com/documentation/file.SASS_REFERENCE.html) which can be helpful for more advanced implementation (interpellation, conditional controls, etc.).

###JavaScript EcmaScript6

The new ES6 syntax for JavaScript is available for use in these files. It is transcribed and polyfilled via [BabelJS](https://babeljs.io/). The dev output includes a sourcemap for debugging the original JS documents in ES6 if applicable.

For an overview of many of the language's new features, [please refer to this features exploration](http://es6-features.org/).

###JS Linting

JavaScript linting through jshint is set up as a separate task. Linting accomodates the ES6 syntax.
```
gulp lint
```

---

##Version History

####1.0
  - First commit
  - To do possibilities
    - Make pug optional!
    - Make ES6/Babel usage optional?
